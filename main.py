from readCSV import readCsv
from verifyData import verifyData

# Instanciando variaveis para fazer a chamada das classes e métodos
csv = readCsv()
conn = verifyData()

# Realizando chamada das Classes
csv.list_files()
csv.select_file()
csv.read_file()
"""
Desenvolvido por: Guilherme Anderson - Analista especialista 3
email: guilherme.anderson34@outlook.com
fone: 12 98704-5093
"""


import glob
import pandas as pd
from verifyData import verifyData


class readCsv:
    def __init__(self):
        self.files = ''
        self.file_name = []
        self.correct_file_name = ''
        self.values = ''
        print(">>> Iniciando processo de identificação do CSV")

    # Lintando arquivos .xlsx encontrados
    def list_files(self):
        self.files = glob.glob("./csv/*.xlsx")
        for file in self.files:
            self.file_name.append(file[6:])
        if len(self.file_name) < 2:
            pass
        else:
            print('>>> Arquivos encontrados:' + str(self.file_name))

    # Pedindo para usuário escolher qual arquivo deseja utilizar para realizar a leitura
    def select_file(self):
        if len(self.file_name) > 1:
            selected = input(' >>> Entre com o nome do arquivo que deseja realizar a leitura: ')
            self.correct_file_name = selected.replace("'", "")
        else:
            self.correct_file_name = self.file_name[0].replace("'", "")
        print(">>> Arquivo selecionado: " + self.correct_file_name)

    # Lendo arquivo escolhido pelo usuário
    def read_file(self):
        data = pd.read_excel('./csv/' + self.correct_file_name)
        self.values = pd.DataFrame(data, columns=['CONTRATO', 'PERC DE REAJUSTE'])
        blankIndex = ['C2'] * len(self.values)
        self.values.index = blankIndex
        print('>>> Leitura do CSV concluida com sucesso!')
        verifyData().selectTxt(self.values, None)
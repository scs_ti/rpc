# RPC
## _Automatização do processo RPC_

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

#### _Script desenvolvido utilizando Python_ 🐍

## _Primeiros Passos_

##### 1. Criar ambiente virtual
###### 1.1. Instalar a virtualenv.

```sh
pip install virtualenv
```

###### 1.2. Criar virtualenv

```sh
virtualenv nome_da_virtualenv
```

###### 1.3. Acessar virtualenv

```sh
cd nome_da_virtualenv/Scripts
Activate
```
##### 2. Instalar dependências do projeto
###### 2.1. Retornar até a raiz do Projeto

```sh
cd ../..
```
###### 2.2. Instalar dependências

```sh
pip install -r requirements.txt
```

##### 3. Arquivos para análise
###### 3.1. Inserir arquivos para leitura dentro da pasta "csv"

## Desenvolvido por:
##### Guilherme Anderson - Alista Especialista III 🧑‍💻  
##### Ramal: 8070 📞
##### Github: github.com/guianderson  
#
#
#
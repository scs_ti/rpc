"""
Desenvolvido por: Guilherme Anderson - Analista especialista 3
email: guilherme.anderson34@outlook.com
Fone: 12 98704-5093
"""


class verifyData:
    def __init__(self):
        self.data = []
        self.cd_contrato = []
        self.b_estudo = []
        self.clean_txt = []
        self.txt_not_base = []
        self.base_not_txt = []

    def selectTxt(self, base_estudo, filename):
        base = base_estudo
        file = filename
        if file is None or file[-4:] != '.txt':
            file = input('\n>>> Entre com o nome do arquivo Gerado pelo MV: ')
            if str(file[-4:]) != '.txt':
                verifyData.filename(self, base_estudo)
            else:
                verifyData.readTxt(self, base, file)
        else:
            verifyData.readTxt(self, base, file)

    def readTxt(self, base, filename):
        file = filename
        # Lendo arquivo de texto e armazenando todas as informações necessárias no Array
        try:
            f = open('csv/' + str(file), 'r')
            for line in f.readlines():
                if str(line[:2]) == 'C2':
                    if str(line[129:130]) == '0':
                        if str(line[132:133]).strip() == '0':
                            self.cd_contrato.append(
                                str(str(line[70:80]).strip() + " " + str(line[130:132]).strip()).strip())
                        else:
                            self.cd_contrato.append(
                                str(str(line[70:80]).strip() + " " + str(line[130:133]).strip()).strip())
                    else:
                        if str(line[132:133]).strip() == '0':
                            self.cd_contrato.append(
                                str(str(line[70:80]).strip() + " " + str(line[129:132]).strip()).strip())
                        else:
                            self.cd_contrato.append(
                                str(str(line[70:80]).strip() + " " + str(line[129:133]).strip()).strip())
        except:
            print('>>> Arquivo não encontrado')
            verifyData.filename(self, base)

        # Retirando valores repetidos
        for contrato in self.cd_contrato:
            if contrato not in self.clean_txt:
                self.clean_txt.append(contrato)
        verifyData.compareData(self, base)

    def filename(self, base_estudo):
        filename = input('>>> Entre com o arquivo gerado pelo MV: ')
        base = base_estudo
        verifyData.selectTxt(self, base, filename)


    def compareData(self, base_estudo):
        base_values  = []
        clean_base   = []

        # Criando String com informações necessárias para realizar comparação
        for index, row in base_estudo.iterrows():
            base_values.append(str('{:.0f}'.format(row['CONTRATO'])) + ' ' + str(row['PERC DE REAJUSTE']).replace('.', ''))

        # Retirando Possiveis valores repetidos
        for i in base_values:
            if i not in clean_base:
                clean_base.append(i)

        # Comparando TXT com CSV
        for i in self.clean_txt:
            if i not in clean_base:
                self.txt_not_base.append(i)

        # Comparando CSV com TXT
        for i in clean_base:
            if i not in self.clean_txt:
                self.base_not_txt.append(i)

        verifyData.information(self)

    def information(self):
        data = {
            1: "Comparando arquivo do MV com Base",
            2: "Comparando Base com arquivo do MV",
            3: "Sair"
        }
        print("Escolha uma opção: ")
        for options in sorted(data):
            print("{} - {}".format(options, data[options]))
        value = input("Opção: ")
        if not value.isdigit() or not int(value) in data:
            verifyData.invalid_insert_value(self, value)
        elif value == "1":
            print(' ------------ Valores do TXT não encontrados na Base ------------ ')
            for i in self.txt_not_base:
                print(i)
            print(' -------------------------- Total: ' + str(len(self.txt_not_base)) + ' -------------------------- ')
            verifyData.new_search(self)
        elif value == "2":
            print(' ------------ Valores da Base não encontrados no TXT ------------ ')
            for i in self.base_not_txt:
                print(i)
            print(' -------------------------- Total: ' + str(len(self.base_not_txt)) + ' -------------------------- ')
            verifyData.new_search(self)
        elif value == "3":
            print("Finalizando software ...")
            exit()

    def invalid_insert_value(self, value):
        print("A opção " + value + " é inválida, por favor, selecione uma opção válida\n")
        verifyData.information(self)

    def new_search(self):
        next = input("\nDeseja continuar? S/n ")
        if next.upper() == 'S':
            verifyData.information(self)
        elif next.upper() == 'N':
            print('\n Finalizando Software...')
        else:
            print('\nResponda apenas com S ou N')
            verifyData.new_search(self)
